import matplotlib.pyplot as plt
import numpy as np
import numpy.random as random

NUMBER_OF_DAYS = 80
START_NUMBER = 100
NUMBER_OF_HAWKS = 0


class Agent:
    """Agent class for the hawk-dove game.

    Attributes:
        type: The type of the agent. 0 is a dove and 1 is a hawk.
        food_eaten: The amount of food eaten by the agent.
    """

    def __init__(self, type_: int) -> None:
        """Initialise."""
        if type_ not in [0, 1]:
            raise ValueError("Type must be 0 or 1.")
        self.type = type_
        self.food_eaten = 0.0
        self.wins = 0

    def _eat(self, food: float) -> None:
        """Add food to the food eaten by the agent."""
        self.food_eaten += food

    def _reset_food(self) -> None:
        """Reset the food eaten by the agent to 0."""
        self.food_eaten = 0.0

    def _check_survival(self) -> bool:
        """Check if the agent has survived."""
        if self.food_eaten >= 1:
            return True
        elif self.food_eaten > 0:
            survive = random.random()
            if survive >= 0.5:
                return True
            else:
                return False
        else:
            return False

    def _check_reproduction(self) -> bool:
        """Check if the agent has reproduced."""
        if self.food_eaten >= 2:
            return True
        elif self.food_eaten > 1.0:
            reproduce = random.random()
            if reproduce > 0.5:
                return True
            else:
                return False
        else:
            return False


def create_food(number_of_food: int) -> dict[int, list[int]]:
    """Create a dictionary of food."""
    dict_of_food: dict[int, list[int]] = {}
    for i in range(number_of_food):
        list_of_agents: list[int] = []
        dict_of_food[i] = list_of_agents
    return dict_of_food


def create_agents(number_of_agents: int) -> list[Agent]:
    """Create a list of agents."""
    list_of_agents: list[Agent] = []
    print(f"Creating agents... {number_of_agents}")
    for i in range(number_of_agents):
        if i < number_of_agents - NUMBER_OF_HAWKS:
            agent_type = 0
        else:
            agent_type = 1
        agent = Agent(agent_type)
        list_of_agents.append(agent)
    return list_of_agents


def create_agents_new_rules(number_of_agents: int) -> list[Agent]:
    """Create a list of agents."""
    list_of_agents: list[Agent] = []
    print(f"Creating agents... {number_of_agents}")
    for i in range(number_of_agents):
        if i < number_of_agents - NUMBER_OF_HAWKS:
            agent_type = 0
        else:
            agent_type = 1
        agent = Agent(agent_type)
        list_of_agents.append(agent)
    return list_of_agents


def move_agents_to_food(
    list_of_agents: list[Agent], dict_of_food: dict[int, list[int]]
) -> None:
    """Move the agents to the food."""
    # Loop through the agents and move them to the food.
    for i in range(len(list_of_agents)):
        # find indecies of food with less than 2 agents
        available_food = [key for key, value in dict_of_food.items() if len(value) < 2]
        # if no food available, break
        if len(available_food) == 0:
            print("No food available")
            break
        # get a random index of the available food
        food_index: int = random.randint(0, len(available_food))
        random_food = available_food[food_index]
        # add the agent to the food
        dict_of_food[random_food].append(i)
    # check the distribution of food
    for _, value in dict_of_food.items():
        check_food_distribution(value, list_of_agents)


def move_agents_to_food_new_rules(
    list_of_agents: list[Agent], dict_of_food: dict[int, list[int]]
) -> None:
    """Allocate agents to food locations.

    Args:
        list_of_agents: A list of agents.
        dict_of_food: A dictionary of food locations.
    """
    for i in range(len(list_of_agents)):
        random_idx: int = random.randint(0, len(dict_of_food.keys()))
        random_foood = list(dict_of_food.keys())[random_idx]
        dict_of_food[random_foood].append(i)
    for _, value in dict_of_food.items():
        check_food_dist_new_rules(value, list_of_agents)


def calculate_winning_hawk(
    agents_at_food: list[int], list_of_agents: list[Agent]
) -> int:
    """Calculate the winning hawk by checking pass wins if tie no one wins."""
    # initialize strongest agent variables
    strongest_value = -1
    strongest_agent_idx = -1
    # list of ties
    ties = []
    # loop through agents at food
    for idx in agents_at_food:
        agent = list_of_agents[idx]
        # if agent is a hawk
        if agent.type == 1:
            # if the current agent has the same number of wins as the strongest agent
            if agent.wins == strongest_value:
                # if there is already a tie, add the current agent to the list of ties
                if strongest_agent_idx in ties:
                    ties.append(idx)
                # otherwise create a new list of ties
                else:
                    ties = [strongest_agent_idx, idx]
            # if the current agent has more wins than the strongest agent
            if agent.wins > strongest_value:
                # if there previously was a tie, clear the list of ties
                if len(ties) > 0:
                    ties = []
                # set the current agent as the strongest agent
                strongest_value = agent.wins
                strongest_agent_idx = idx
    # if there is a tie, return -1
    if len(ties) > 0:
        return -1
    else:
        # iterate the wins of the strongest agent
        list_of_agents[strongest_agent_idx].wins += 1
        # return the index of the strongest agent
        return strongest_agent_idx


def check_food_dist_new_rules(
    agents_at_food: list[int], list_of_agents: list[Agent]
) -> None:
    """Check the agens at a specific food location and determine the outcome.

    Args:
        agents_at_food: A list of agents at a specific food location.
        list_of_agents: A list all of agents.
    """
    if len(agents_at_food) == 0:
        return
    # get the agents at the food
    agents = [list_of_agents[i] for i in agents_at_food]
    # get the indices of the hawks
    idx_of_hawks = [idx for idx in agents_at_food if list_of_agents[idx].type == 1]
    # get the number of doves
    num_doves = len(agents) - len(idx_of_hawks)
    # get random values for the fights between hawks
    # get the best value
    if len(idx_of_hawks) != 0:
        if len(idx_of_hawks) == 1:
            winning_hawk_idx = idx_of_hawks[0]
            list_of_agents[winning_hawk_idx].wins += 1
        else:
            winning_hawk_idx = calculate_winning_hawk(idx_of_hawks, list_of_agents)
        for idx in agents_at_food:
            # if the agent is a hawk it only eats if it wins the fight
            if list_of_agents[idx].type == 0:
                # if there are hawks the doves eat 1/num_doves
                list_of_agents[idx]._eat(1 / num_doves)
            elif idx == winning_hawk_idx:
                if num_doves == 0:
                    list_of_agents[idx]._eat(2)
                # otherwise the hawk eats 1
                else:
                    list_of_agents[idx]._eat(1)
    else:
        for i in range(len(agents)):
            agents[i]._eat(2 / num_doves)


def check_food_distribution(
    agents_at_food: list[int], list_of_agents: list[Agent]
) -> None:
    """Check the distribution of food."""
    if len(agents_at_food) == 0:
        return
    if len(agents_at_food) == 1:
        list_of_agents[agents_at_food[0]]._eat(2)
        return
    if len(agents_at_food) > 2:
        raise ValueError("More than 2 agents at food.")
    # unpack the agents
    agent_1_idx = agents_at_food[0]
    agent_2_idx = agents_at_food[1]
    agent_1 = list_of_agents[agent_1_idx]
    agent_2 = list_of_agents[agent_2_idx]
    # If both agents are pigeons
    if agent_1.type == 0 and agent_2.type == 0:
        list_of_agents[agent_1_idx]._eat(1)
        list_of_agents[agent_2_idx]._eat(1)
    # If one agent is a hawk and one is a pigeon
    elif agent_1.type == 0 and agent_2.type == 1:
        list_of_agents[agent_1_idx]._eat(0.5)
        list_of_agents[agent_2_idx]._eat(1.5)
    elif agent_1.type == 1 and agent_2.type == 0:
        list_of_agents[agent_1_idx]._eat(1.5)
        list_of_agents[agent_2_idx]._eat(0.5)
    # If both agents are hawks do nothing


def plot_population(
    list_of_hawks: list[int], list_of_doves: list[int], new_rules: bool = False
) -> None:
    """Plot the population of hawks and doves."""
    x_axis = [i for i in range(NUMBER_OF_DAYS + 1)]
    plt.bar(x_axis, list_of_doves, label="Doves")
    plt.bar(x_axis, list_of_hawks, bottom=list_of_doves, label="Hawks")
    plt.xlabel("Days")
    plt.ylabel("Population")
    plt.title(
        f"Predator-Prey Simulation with Starting with {START_NUMBER} "
        f"of agents and {NUMBER_OF_HAWKS} Hawks."
    )
    plt.legend()
    if new_rules:
        plt.savefig(f"figures/prey_predator_{NUMBER_OF_HAWKS}_new_rules.png")
    else:
        plt.savefig(f"figures/prey_predator_{NUMBER_OF_HAWKS}.png")
    plt.cla()


def execute_day_loop() -> None:
    """Execute a day of the simulation."""
    # Initialize the lists of hawks and doves
    list_of_agents = create_agents(START_NUMBER)
    # Create the food
    dict_of_food = create_food(START_NUMBER // 2)
    number_of_init_hawks = len([agent for agent in list_of_agents if agent.type == 1])
    number_of_init_doves = START_NUMBER - number_of_init_hawks
    number_of_hawks: list[int] = [number_of_init_hawks]
    number_of_doves: list[int] = [number_of_init_doves]
    # loop through the days
    for i in range(NUMBER_OF_DAYS):
        # move the agents to the food
        move_agents_to_food(list_of_agents, dict_of_food)
        list_of_new_agents: list[Agent] = []
        list_to_pop: list[int] = []
        for j, agent in enumerate(list_of_agents):
            # Check if the agent survives
            survive: bool = agent._check_survival()
            # if the agent survives, check if it reproduces
            if survive:
                reproduce: bool = agent._check_reproduction()
                agent._reset_food()
                # If the agent reproduces, create a new agent
                if reproduce:
                    new_agent = Agent(agent.type)
                    list_of_new_agents.append(new_agent)
            else:
                # If the agent does not survive, add the index to the list of
                # agents to remove
                list_to_pop.append(j)

        # Remove the agents that did not survive
        new_list_of_agents = [
            list_of_agents[agent_idx]
            for agent_idx in range(len(list_of_agents))
            if agent_idx not in list_to_pop
        ]
        list_of_agents = new_list_of_agents
        # add the new agents to the list of agents
        list_of_agents = list_of_agents + list_of_new_agents
        # clear the food
        for _, food in dict_of_food.items():
            food.clear()
        # get the population of hawks and doves
        number_of_hawks.append(
            len([agent for agent in list_of_agents if agent.type == 1])
        )
        number_of_doves.append(
            len([agent for agent in list_of_agents if agent.type == 0])
        )
    plot_population(number_of_hawks, number_of_doves)


def execute_day_loop_new_rules() -> None:
    """Execute a day of the simulation."""
    # Initialize the lists of hawks and doves
    list_of_agents = create_agents_new_rules(START_NUMBER)
    # Create the food
    dict_of_food = create_food(START_NUMBER // 2)
    number_of_init_hawks = len([agent for agent in list_of_agents if agent.type == 1])
    number_of_init_doves = START_NUMBER - number_of_init_hawks
    number_of_hawks: list[int] = [number_of_init_hawks]
    number_of_doves: list[int] = [number_of_init_doves]
    # loop through the days
    for i in range(NUMBER_OF_DAYS):
        # move the agents to the food
        move_agents_to_food_new_rules(list_of_agents, dict_of_food)
        list_of_new_agents: list[Agent] = []
        list_to_pop: list[int] = []
        for j, agent in enumerate(list_of_agents):
            # Check if the agent survives
            survive: bool = agent._check_survival()
            # if the agent survives, check if it reproduces
            if survive:
                reproduce: bool = agent._check_reproduction()
                agent._reset_food()
                # If the agent reproduces, create a new agent
                if reproduce:
                    new_agent = Agent(agent.type)
                    list_of_new_agents.append(new_agent)
            else:
                # If the agent does not survive, add the index to the list of
                # agents to remove
                list_to_pop.append(j)

        # Remove the agents that did not survive
        new_list_of_agents = [
            list_of_agents[agent_idx]
            for agent_idx in range(len(list_of_agents))
            if agent_idx not in list_to_pop
        ]
        list_of_agents = new_list_of_agents
        # add the new agents to the list of agents
        list_of_agents = list_of_agents + list_of_new_agents
        # clear the food
        for _, food in dict_of_food.items():
            food.clear()
        # get the population of hawks and doves
        number_of_hawks.append(
            len([agent for agent in list_of_agents if agent.type == 1])
        )
        number_of_doves.append(
            len([agent for agent in list_of_agents if agent.type == 0])
        )
    plot_population(number_of_hawks, number_of_doves, True)


if __name__ == "__main__":
    for i in range(12):
        execute_day_loop()
        execute_day_loop_new_rules()
        if i == 0:
            NUMBER_OF_HAWKS += 1
        elif i == 1:
            NUMBER_OF_HAWKS += 9
        else:
            NUMBER_OF_HAWKS += 10
