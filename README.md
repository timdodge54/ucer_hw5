# Uncertainty in the Predator-Prey Model: Homework 5

# Must have python3.10 or higher installed

## Installing prerequisites
`pip3 install -r requirements.txt`


## Running the program
`python3 aggression.py`

# Part 1
The following show the results of the predator and prey populations over 80 days. 

![prey_predator0](figures/prey_predator_0.png)

![prey_predator1](figures/prey_predator_1.png)

![prey_predator10](figures/prey_predator_10.png)

![prey_predator20](figures/prey_predator_20.png)

![prey_predator30](figures/prey_predator_30.png)

![prey_predator40](figures/prey_predator_40.png)

![prey_predator50](figures/prey_predator_50.png)

![prey_predator60](figures/prey_predator_60.png)

![prey_predator70](figures/prey_predator_70.png)

![prey_predator80](figures/prey_predator_80.png)

![prey_predator90](figures/prey_predator_90.png)

![prey_predator100](figures/prey_predator_100.png)

# Part 2
The following shows the results of the predator and prey populations over 80 days with a changed ruleset.

![prey_predator_0_new_rules](figures/prey_predator_0_new_rules.png)

![prey_predator_1_new_rules](figures/prey_predator_1_new_rules.png)

![prey_predator_10_new_rules](figures/prey_predator_10_new_rules.png)

![prey_predator_20_new_rules](figures/prey_predator_20_new_rules.png)

![prey_predator_30_new_rules](figures/prey_predator_30_new_rules.png)

![prey_predator_40_new_rules](figures/prey_predator_40_new_rules.png)

![prey_predator_50_new_rules](figures/prey_predator_50_new_rules.png)

![prey_predator_60_new_rules](figures/prey_predator_60_new_rules.png)

![prey_predator_70_new_rules](figures/prey_predator_70_new_rules.png)

![prey_predator_80_new_rules](figures/prey_predator_80_new_rules.png)

![prey_predator_90_new_rules](figures/prey_predator_90_new_rules.png)

![prey_predator_100_new_rules](figures/prey_predator_100_new_rules.png)

# Part 3

For the all doves case there is some variation in the doves population in the second section where the first section the population is constant. In the all hawks case the population immediatly dies where in the second case the hawks population is able to sustain it self since hawks can win and not always lose against each other. In the mixed cases the hawks and dove population in the second seem more stable where it seems the hawks population seems to kill more of the doves population in the first case. 